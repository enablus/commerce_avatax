<?php

// $Id: commerce_avatax_ui.module,v 1.2 2012/03/09 18:00:00 adTumbler Exp $

/**
 * @file
 * Admin settings for commerce_avatax.
 *
 * Copyright (C) Alexander Bischoff, adTumbler.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 */

/**
 * Admin settings menu callback.
 */

function commerce_avatax_ui_admin_settings() {

  $form = array();

  // Set AvaTax license details
  $form['commerce_avatax_product_version'] = array(
    '#title' => t('Select AvaTax Version'),
    '#description' => t('Select AvaTax Calc Basic or AvaTax Calc Pro according to your AvaTax Acount License'),
    '#type' => 'select',
    '#options' => array(
      'basic' => t('AvaTax Calc Basic'),
      'pro' => t('AvaTax Calc Pro')),
    '#default_value' => variable_get('commerce_avatax_product_version', 'basic'),
  );

  $form['commerce_avatax_company_code'] = array(
    '#title' => t('Company Code'),
    '#description' => t('Enter the Company Code found under Organization in your AvaTax Dashboard'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_avatax_company_code', ''),
  );

  $form['commerce_avatax_use_mode'] = array(
    '#title' => t('Select Development or Production'),
    '#description' => t('Select Development - Only select Production if you have completed the GO LIVE process with Avalara'),
    '#type' => 'select',
    '#options' => array(
      'Development' => t('Development'),
      'Production' => t('Production')),
    '#default_value' => variable_get('commerce_avatax_use_mode', 'Development'),
  );

  // Select list of states to send request to AvaTax
  $form['commerce_avatax_select_states'] = array(
    '#title' => t('Limit AvaTax Calc to selected states'),
    '#description' => t('Enter list of states - e.g. CA, NY, WA - Leave blank for all states'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_avatax_select_states', ''),
  );

  // Set Shipping Tax code to be used by Avatax
  $form['commerce_avatax_shipcode'] = array(
    '#title' => t('Shipping Tax Code'),
    '#description' => t('The Sales Tax code to be used for Shipping'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_avatax_shipcode', 'FR020100'),
  );

  // Set Sales Tax Descriptions to be shown to users
  $form['commerce_avatax_tax_description'] = array(
    '#title' => t('Sales Tax Description'),
    '#description' => t('The Sales Tax description to be displayed on the order check out form'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_avatax_tax_description', 'Sales Tax'),
  );

  $form['commerce_avatax_show_loc'] = array(
    '#title' => t('Show location code'),
    '#description' => t('Select Yes to include the City name in your Sales Tax description'),
    '#type' => 'radios',
    '#options' => array('0' => t('No'), '1' => t('Yes')),
    '#default_value' => variable_get('commerce_avatax_show_loc', '1'),
  );

  // Set Address to use for Sales Tax
  $form['commerce_avatax_billing_address'] = array(
    '#title' => t('Select Destination Address to use for Sales Tax'),
    '#description' => t('Only select Shipping if you have installed the Drupal Commerce Shipping Module'),
    '#type' => 'select',
    '#options' => array(
      'Billing' => t('Billing'),
      'Shipping' => t('Shipping')),
    '#default_value' => variable_get('commerce_avatax_billing_address', 'Billing'),
  );

   // Set Sales Tax automatic commits
   $form['commerce_avatax_erp_status'] = array(
    '#title' => t('Automatic updates to AvaTax doc status'),
    '#description' => t('Select "Yes" to enable automatic updates to AvaTax doc status'),
    '#type' => 'radios',
    '#options' => array('0' => t('No'), '1' => t('Yes')),
    '#default_value' => variable_get('commerce_avatax_erp_status', '0'),
  );

  // Set Street, City, State and Zip for Primary Business Office Location
  $form['commerce_avatax_primary_street1'] = array(
    '#title' => t('Primary Business Street 1'),
    '#description' => t('The Primary Street 1 your business is located in'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_avatax_primary_street1', ''),
  );

  $form['commerce_avatax_primary_street2'] = array(
    '#title' => t('Primary Business Street 2'),
    '#description' => t('The Primary Street 2 your business is located in'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_avatax_primary_street2', ''),
  );

  $form['commerce_avatax_primary_city'] = array(
    '#title' => t('Primary Business City'),
    '#description' => t('The primary City your business is located in'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_avatax_primary_city', ''),
  );

  $form['commerce_avatax_primary_state'] = array(
    '#title' => t('Primary Business State'),
    '#description' => t('The primary State your business is located in. NB - Must be a Valid 2 character code'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_avatax_primary_state', ''),
  );

  $form['commerce_avatax_primary_zip'] = array(
    '#title' => t('Primary Business Zip'),
    '#description' => t('The primary Zip code your business is located in. NB - Must be a Valid 5 digit zip'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_avatax_primary_zip', ''),
  );

  // Sales Tax Exemption management
  $form['commerce_avatax_exemptions'] = array(
    '#title' => t('Use AvaTax Calc for Sales Tax Exemptions'),
    '#description' => t('Leave as Yes - Future provision for a sales tax exemption management module'),
    '#type' => 'radios',
    '#options' => array('0' => t('No'), '1' => t('Yes')),
    '#default_value' => variable_get('commerce_avatax_exemptions', '1'),
  );

  return system_settings_form($form);
}