<?php

// $Id: commerce_avatax_pro.inc, v 1.2 2012/06/08 18:00:00 adTumbler Exp $

/**
 * @file
 * Avalara GetTax amount.
 *
 */


/**
 * Gets the tax amount for the order based on the delivery address.
 *
 * @param $order
 *   The current order object.
 *
 * @param $ava_args
 *   An array containing from & to delivery details.
 *
 * @return
 *   FALSE if the tax calculation failed.
 *   An array containing tax amount, taxable amount, total order amount - if success
 */

function commerce_avatax_get_tax($order, $order_wrapper, $ava_args) {

  // include in all Avalara Scripts
  require_once('ava_tax.php');
  require_once('credentials.php');

  // Avatax Get Tax rates
  // Create new origin, destination, client and tax request objects

  $client = new TaxServiceSoap($ava_args['use_mode']);
  $request = new GetTaxRequest();
  $origin = new Address();
  $destination = new Address();

  // Populate the From address
  $origin->setLine1($ava_args['primary_street1']);
  $origin->setLine2($ava_args['primary_street2']);
  $origin->setCity ($ava_args['primary_city']);
  $origin->setRegion($ava_args['primary_state']);
  $origin->setPostalCode($ava_args['primary_zip']);

  // Populate the Destination address
  $destination->setLine1($ava_args['street1']);
  $destination->setLine2($ava_args['street2']);
  $destination->setCity($ava_args['city']);
  $destination->setRegion($ava_args['state']);
  $destination->setCountry($ava_args['country']);
  $destination->setPostalCode($ava_args['zip']);

  // Set request object
  $request->setOriginAddress($origin);
  $request->setDestinationAddress ($destination);
  $request->setcompanycode($ava_args['company_code']);
  $request->setDetailLevel(DetailLevel::$Tax);

  $request->setDocType('SalesInvoice');

  $dateTime=new DateTime();

  if ($order->order_id == 0) {
    $request->setDocType('SalesOrder');
  }
  else {
    $request->setDocType('SalesInvoice');
  }

  $request->setDocCode('dc-' . $order->order_id . ''); //Update "dc-" to amend doc id in Avatax - remember to update .erp to match this!

  $request->setDocDate(date_format($dateTime, "Y-m-d"));
  $request->setSalespersonCode(variable_get('site_name', 'Drupal'));
  $request->setCustomerCode($ava_args['user_id']);
  $request->setLocationCode('');
  $request->setCustomerUsageType('');

  $shipping_amount = 0;
  $coupon_amount = 0;

  $pro_mode = 'configure'; // Required: Change 'configure' to one of the following modes - 'compute' 'item_code' 'tax_code'

  // Return error if AvaTax Pro has been selected in Avatax Calc administration but $pro_mode above has not been selected
  if ($pro_mode == 'configure') {
    drupal_set_message(t('Drupal Commerce Connector for AvaTax error. Selection of Avatax Pro requires (line 83) $pro_mode to be configured.'), 'error');
    $tax_data = array();
    return $tax_data;
  }

  $i = 1;

  // To Compute Sales Tax codes - Sales Tax Code based on skew being matched in array sent to AvaTax - No match will default to " "
  if ($pro_mode == 'compute') {
    foreach ($order_wrapper->commerce_line_items as $line => $line_item_wrapper) {
      $line_item = $line_item_wrapper->value();
      if (in_array($line_item->type, commerce_product_line_item_types())) {
        // If the skew ($line_item->line_item_label) matches a value in the array - the line item will not be taxed for tax code - Not Taxable NT
        if (in_array($line_item->line_item_label, array('NT-Skew1','NT-Skew2'))) {
          $tax_code = 'NT'; //Non Taxable Product
          ${'line'. $i} = new Line();
          ${'line'. $i}->setNo ($i);
          ${'line'. $i}->setItemCode($line_item->line_item_label);
          ${'line'. $i}->setDescription($line_item_wrapper->commerce_product->title->value());
          ${'line'. $i}->setTaxCode($tax_code);
          ${'line'. $i}->setQty($line_item->quantity);
          ${'line'. $i}->setAmount(($line_item_wrapper->commerce_unit_price->amount->value()/100)*$line_item->quantity);
          ${'line'. $i}->setDiscounted('false');
          ${'line'. $i}->setRevAcct('');
          ${'line'. $i}->setRef1('');
          ${'line'. $i}->setRef2('');
          ${'line'. $i}->setCustomerUsageType('');
          $lines[] = ${'line'. $i};
          $i++;
        }
        // If the skew ($line_item->line_item_label) matches a value in the array - the line item will be taxed for tax code - value of $tax_code
        elseif (in_array($line_item->line_item_label, array('CUST-Skew1','CUST-Skew2'))) {
          $tax_code = 'CUST0100';  //Edit as required - a custom tax code or an AvaTax tax code
          ${'line'. $i} = new Line();
          ${'line'. $i}->setNo ($i);
          ${'line'. $i}->setItemCode($line_item->line_item_label);
          ${'line'. $i}->setDescription($line_item_wrapper->commerce_product->title->value());
          ${'line'. $i}->setTaxCode($tax_code);
          ${'line'. $i}->setQty($line_item->quantity);
          ${'line'. $i}->setAmount(($line_item_wrapper->commerce_unit_price->amount->value()/100)*$line_item->quantity);
          ${'line'. $i}->setDiscounted('false');
          ${'line'. $i}->setRevAcct('');
          ${'line'. $i}->setRef1('');
          ${'line'. $i}->setRef2('');
          ${'line'. $i}->setCustomerUsageType('');
          $lines[] = ${'line'. $i};
          $i++;
        }
        else {
        // If no earlier matches - the line item will be taxed for tax code - Tangible Personal Property P0000000
          $tax_code = '';
          ${'line'. $i} = new Line();
          ${'line'. $i}->setNo ($i);
          ${'line'. $i}->setItemCode($line_item->line_item_label);
          ${'line'. $i}->setDescription($line_item_wrapper->commerce_product->title->value());
          ${'line'. $i}->setTaxCode($tax_code);
          ${'line'. $i}->setQty($line_item->quantity);
          ${'line'. $i}->setAmount(($line_item_wrapper->commerce_unit_price->amount->value()/100)*$line_item->quantity);
          ${'line'. $i}->setDiscounted('false');
          ${'line'. $i}->setRevAcct('');
          ${'line'. $i}->setRef1('');
          ${'line'. $i}->setRef2('');
          ${'line'. $i}->setCustomerUsageType('');
          $lines[] = ${'line'. $i};
          $i++;
        }
      }
    }
  }

  // To assign an Item Code from a custom field in the product node - Item Code mapped to Sales Tax Code in AvaTax as required
  if ($pro_mode == 'item_code') {
  // functionality to be added !
  }

  // Module will assign an Item Code from a custom field in the product node - Item Code mapped to Sales Tax Code in AvaTax as required
  if ($pro_mode == 'tax_code') {
  // functionality to be added !
  }

  foreach ($order_wrapper->commerce_line_items as $line => $line_item_wrapper) {
    $line_item = $line_item_wrapper->value();
    if (in_array($line_item->type, array('shipping'))) {
      ${'line'. $i} = new Line();
      ${'line'. $i}->setNo ($i);
      ${'line'. $i}->setItemCode('Shipping');
      ${'line'. $i}->setDescription('Shipping');
      ${'line'. $i}->setTaxCode($ava_args['shipcode']);
      ${'line'. $i}->setQty($line_item->quantity);
      ${'line'. $i}->setAmount(($line_item_wrapper->commerce_unit_price->amount->value()/100)*$line_item->quantity);
      ${'line'. $i}->setDiscounted('false');
      ${'line'. $i}->setRevAcct('');
      ${'line'. $i}->setRef1('');
      ${'line'. $i}->setRef2('');
      ${'line'. $i}->setCustomerUsageType('');
      $lines[] = ${'line'. $i};
      $shipping_amount += ($line_item_wrapper->commerce_unit_price->amount->value()/100)*$line_item->quantity;
      $i++;
    }
    elseif (in_array($line_item->type, array('coupon'))) {
      ${'line'. $i} = new Line();
      ${'line'. $i}->setNo ($i);
      ${'line'. $i}->setItemCode('Coupon');
      ${'line'. $i}->setDescription('Coupon');
      ${'line'. $i}->setTaxCode('OD010000');
      ${'line'. $i}->setQty($line_item->quantity);
      ${'line'. $i}->setAmount(($line_item_wrapper->commerce_unit_price->amount->value()/100)*$line_item->quantity);
      ${'line'. $i}->setDiscounted('false');
      ${'line'. $i}->setRevAcct('');
      ${'line'. $i}->setRef1('');
      ${'line'. $i}->setRef2('');
      ${'line'. $i}->setCustomerUsageType('');
      $lines[] = ${'line'. $i};
      $coupon_amount = ($line_item_wrapper->commerce_unit_price->amount->value()/100)*$line_item->quantity;
      $i++;
    }
  }

  $request->setLines($lines);

  // Try AvaTax

  try {
    $getTaxResult = $client->getTax($request);
    if ($getTaxResult->getResultCode() == SeverityLevel::$Success) {
      $tax_data = array(
        'tax_amount' => $getTaxResult->getTotalTax(),
        'taxable_amount' => $getTaxResult->getTotalTaxable(),
        'total_amount' => $getTaxResult->getTotalAmount(),
        'shipping_amount' => $shipping_amount,
        'coupon_amount' => $coupon_amount,
     );
    }
    else {
      foreach ($getTaxResult->getMessages() as $msg) {
        drupal_set_message(t('AvaTax error: ' . $msg->getName() . ": " . $msg->getSummary() . ''), 'error');
      }
    return FALSE;
    }
  }

  catch (SoapFault $exception) {
    $msg = 'SOAP Exception: ';
    if ($exception) {
      $msg .= $exception->faultstring;
    }
    drupal_set_message(t('AvaTax message is: ' . $msg . '.'));
    drupal_set_message(t('AvaTax last request is: ' . $client->__getLastRequest() . '.'));
    drupal_set_message(t('AvaTax last response is: ' . $client->__getLastResponse() . '.'));
    return FALSE;
  }
  return $tax_data;
}