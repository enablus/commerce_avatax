$Id: README.txt, version 1.2 2012/03/09 18:00:00 adTumbler Exp $

TERMS OF USE
============
The AvaTax SDK supplied with this module is NOT PROVIDED under the same GNU terms of the Drupal Commerce Connector for AvaTax Calc� module.

You are expressly not authorized to copy or distribute any part of the  Avalara, Inc proprietary software property.

For terms of service - please refer AvaTax.txt
For use of the AvaTax SDK - refer - http://developer.avalara.com

INTRODUCTION
============
The Drupal Commerce Connector for AvaTax Calc� (AvaTax) is a Drupal compliant module that integrates the Drupal Commerce check-out process with AvaTax from Avalara, Inc for the management of sales tax calculations and sales tax compliance. AvaTax reduces the audit risk to a company with a cloud-based sales tax services that makes it simple to do rate calculation while managing exemption certificates, filing forms and remitting payments.  

The module supports two modes - Basic or Pro - which correspond to the two levels of service available for AvaTax. 

The AvaTax Basic service offers limited use of the AvaTax sales tax codes, in particular P0000000 for Personal Taxable goods, Shipping tax codes and NT for Not Taxable goods.

The AvaTax Pro service offeres full use of the AvaTax sales tax codes. The pro service is required for states like New York were sales tax rates can be based on the type of product, the delivery address, and the time of year.

Sales Tax is calculated based on the delivery address, the sales tax codes assigned to line item in the order, and the sales tax rules applicable to the states in which nexus has been set up.

The AvaTax cloud service is managed using the AvaTax dasboard. Access to a full functional development account is provided after registration on the Avalara web site.

AvaTax Development service: https://admin-development.avalara.net
AvaTax Production service: https://admin-avatax.avalara.net

Version 1.2 of the module is compatible with Drupal 7.x and has been tested with Drupal Commerce 1.2

REQUIREMENTS
============
a) The Web Server must be configured to support SOAP

The module install file will check for SOAP and report the result to the Administrator Status report. Administrators are requested to check the Status report after enabling the module to ensure SOAP is showing as enabled for AvaTax

b) We strongly recommend that you do NOT try and automate sales tax transaction updates until after the module has been installed, and the administrator has become familiar with the modules functionality.

NB: This SAMPLE CODE allows the module to be extended to support sales order processing integration. Please read the support forum post on this topic prior to attempting to use this feature.

http://drupalsalestax.com/forum/support-configuration/automate-avatax-updates-posted-committed  

c) AvaTax SDK - PHP Libraries

The AvaTax SDK class libraries must be included as described in the installation notes below.

The module is supplied with a pre-configured credentials file and license key - refer credentials.php - Using the module in "Development" mode will allow the module to connect to the development account provided. 

Please note the 120 development transaction limit. Contact us if additional resource is required!

NEW INSTALLATION
=================
Installing the module is done in the same way as any custom Drupal Commerce module

a) Unzip & copy the folder "commerce_avatax" to the location shown below - or in accordance with your Drupal Commerce configuration.

yoursite/sites/all/modules/commerce/modules/commerce_avatax

b) The classes folder is supplied as a sub directory and must remain there.

yoursite/sites/all/modules/commerce/modules/commerce_avatax/classes

c) Enable the two modules (connector and UI) in the normal way for a Drupal Drupal Commerce module. 

The two modules appear in the section Commerce AvaTax Calc

d) Select Reports -> Status Report: Make sure you can see 'SOAP Enabled for AvaTax' in the Status report

e) To ping the SOAP service - optional test.

Enter following url into browser window - standard install: http://yoursite/sites/all/modules/commerce/modules/commerce_avatax/commerce_avatax_ping.php

Ping ResultCode is: Success Ping Version is: 12.1.1.1 

CONFIGURATION
=============
a) Select -> Store -> Configuration -> Taxes -> Tax Types
   Select Add a tax type
   
   Title = avatax (The machine name created MUST be "avatax")

   Display title = Sales Tax
   Do not round tax amounts

   Save the tax type

Note: Creating this tax type will automatically set up the rules required to invoke the hooks needed for sales tax calculations - do not alter these!

b) Select Store -> Configuration -> AvaTax Calc - Sales Tax settings

Complete the information requested after reading the following notes.
Save the form - AvaTax Calc - Sales Tax settings - on completion.

1) Select AvaTax Version: Select AvaTax Calc Basic or AvaTax Calc Pro according to your AvaTax Acount License. It is possible to use the Pro version of the module, with the Avatax basic service. This is done when setting up custom tax codes in AvaTax, and using the module to determine which sales tax codes to allocate to a line item.

2) Company Code: The AvaTax company code is found in the AvaTax development account. Please make sure you have configured Nexus for the state of the web site being tested. You are encouraged to configure the Nexus according to your clients's requirements.

3) Select Development or Production: Only select Production if you have completed the GO LIVE process with Avalara and aded the AvaTax production account number and license key to the file - credentials.php

4) Limit AvaTax Calc to selected states: The module allows limiting  sales tax calculations by AvaTax to explicity listed states. The default admininstrative option is for this field to be empty. This will result in all orders being sent to the AvaTax service on checkout. It is important that you consult with an accountant or sales tax advisor on your legal obligations to collect Sales Tax - known as Sales Tax Nexus. The module will co-exist with sales tax rates configured using the regular Drupal Commerce sales tax module.

5) Shipping Tax Code: FR020100 is the AvaTax sales tax code for shipping by public carrier (USPS, Fedex, etc)- without chnage to their rates - please refer to Avalara for the correct codes if you have a different shipping method or billing model. The module only caters for one shipping sales tax code. Please contact us if the store has multiple shipping models. The Pro version of the module can be extended to support these requirements.   

6) Sales Tax Description: The description of the sales tax line item. 

7) Show location code: If selected the sales tax description will include the delivery city entered in the shipping address field at checkout.  

8) Show zero taxes: Will display zero for shipmenst to states where sales tax is not added.  

9) Use Billing Address for Sales Tax: Select NO - unless your site ONLY sells digital goods. Sales Tax regulations state that the delivery address of digital goods is the customers billing address. If your site sells both digital and physical goods, please select NO. 

If the site allows a combination of physical and digital goods on one order please contact us to explain how best to extend the pro version of the module.

10) Automatic updates to AvaTax doc status: Refer best practises - do not select YES until after you have processed your first transactions, and read up on best practises.

11) Primary Address: NB - Sales tax law requires that a sales tax transaction record the place from which good are shipped. This version of the module does not support Drop Shipments. However, the module will function correctly so long as a valid head office address is entered. 

You MUST enter a valid address in this section of the administrative form. 

12) Use AvaTax Calc for Sales Tax Exemptions: This option has been included as a place holder only. It is STRONGLY suggested that you use the Avatax exemption management system to administer sales tax exemptions.

OPERATION OF MODULE
===================
a) Delete or make inactive any Sales Tax rates configured for the states you will be collecting sales tax using the AvaTax service.

b) There is no test connectivity button on the admin form yet. The best way to test connectivity is to process an order with a delivery address in a state where Nexus has been configured. Visually inspect the cart for inclusion of a sales tax amount after entering the shipping information and proceeding to the next step.

A SOAP error will (caused by an incorrectly configured account/license key) will be clearly displayed on the check out form.   

c) The sales tax transaction will be added to the AvaTax cloud service at the same time that Drupal Commerce allocates an order number to the order object. This is done in order to synchronize the Drupal Commerce order number with the AvaTax transaction.

d) The module is designed to co-exist with the Drupal Commerce sales tax module. Refer to the administrative configuration for use cases where AvaTax is used to manage sales tax for selected states only.

MATCHING LINE ITEMS TO SALES TAX CODES - BASIC SERVICE 
======================================================
The computation of Sales Tax Rates and the Sales Tax Amount is determined by the configuration of the AvaTax account, in particular Nexus configuration.

All Product line items default to the tax code - P0000000 - Personal Taxable Goods.

The Discount coupon line item in the order defaults to the sales tax code - OD010000 - discount on Personal Taxable Goods.

The Shipping product line item is defined by the sales tax administrator - default - FR020100 - Public Carrier at prices quoted.

The AvaTax cloud service allows the creation of custom Tax Codes, Tax rules and the asignment (Items) of Drupal Commerce product codes to a sales tax code. Please refer to a sales tax advisor for sales tax compliance and to AvaTax for technical support.

If you supply Taxable Products that are NOT Tangible Personal Property, you are required to set up your own Tax Codes and Tax Rules for these products. (See Pro service for use of AvaTax Tax Codes and Tax Rules) The Item function in the dashboard is used to map each taxable Product Code established in the Drupal Commerce calalog (that is NOT Tangible personal Property) to the Tax Code supporting it's tax rules and rates.

If you supply Products that are EXEMPT of sales tax in SOME states you are required to set up your own Tax Codes and Tax Rules for these products. (See Pro service for use of AvaTax Tax Codes and Tax Rules) The Item function in the dashboard is used to map each EXEMPT Product Code established in the Drupal Commerce calalog to the Tax Code supporting it's tax rules and rates.

NB: If you supply Products that are EXEMPT of sales tax in all states where you have Nexus (collect sales tax) you may use the Item function in the dashboard to map each EXEMPT Product Code established in the Drupal Commerce calalog to the AvaTax Tax Code - NT - Non Taxable Goods

MATCHING LINE ITEMS TO SALES TAX CODES - PRO SERVICE 
====================================================
The computation of Sales Tax Rates and the Sales Tax Amount is determined by the configuration of the AvaTax account, in particular Nexus configuration.

The file - commerce_avatax_pro.inc - starting at line 82 - has been commented to explain how a developer can customize the connector to select an AvaTax Sales Tax Code based on the Product Code in the Drupal Commerce Catalog.

A Product Line NOT processed using the custom logic defined in the Pro version of the module will default to the tax code - P0000000 - Personal Taxable Goods.

The Discount coupon line item in the order defaults to the sales tax code - OD010000 - discount on Personal Taxable Goods.

The Shipping product line item is defined by the sales tax administrator - default - FR020100 - by Public Carrier

If you supply Taxable Products that are NOT Tangible Personal Property, and DO have a matching AvaTax tax code, the Item function in the dashboard is used to map each taxable Product Code established in the Drupal Commerce calalog to the correct AvaTax Tax Code. Each AvaTax Tax Code is internally configured to apply the correct rates for all 50 states. 

If you supply Products that are EXEMPT of sales tax in some states, but not others, like digital goods - the Item function in the dashboard is used to map each EXEMPT Product Code established in the Drupal Commerce calalog to the correct AvaTax Tax Code. Each AvaTax Tax Code is internally configured to determine if an item is exempt in that state.

The AvaTax dashboard allows the creation of custom Tax Codes, Tax rules and the asignment (Items) of Drupal Commerce product codes to the supported sales tax codes. Please refer to a sales tax advisor for compliance requirements determining a need for custom Tax Codes and Tax Rules, and to the Avatax training for technical support.

SALES ORDER PROCESSING
======================
The Connector does NOT provide automated sales order processing integration with Drupal Commerce

There are a number of ways to implement this, from manual administration (standard) to custom integration to integration with SAGE or Quickbooks using the AvaTax connectors they supply.

Note: If an order is edited in Drupal Commerce, please check that you have not bypassed the sales tax calculation function in the order module. If you do bypass this function, the sales tax will not be correctly re-calculated and the AvaTax transaction updated.

Note: If an order is cancelled in Drupal Commerce, it is suggested that the sales tax administrator void the transaction in AvaTax. It is also suggested that the order item is NOT deleted in Drupal Commerce. Although it is technically possible to delete an order from the database, there are sales tax compliance requirements to be considered. 

It is strongly suggested that a financial controller, accountant, or legal consultant is engaged to advise you about sales tax compliance.

PRODUCTION ACCOUNT - GO LIVE PROCESS
====================================
The file - credentials.php - requires an Avalara account number and the Avalara license key. 

The file is supplied pre-configured with a development profile, and a production profile. The development profile includes the development account number and license key for the adTumbler development Avatax account. The production profile is "blank" and must be populated in order to use the customers AvaTax account.

NB: The production account number and license key will be supplied to the sales tax administrator after they have completed the Go Live training call with Avalara. 

To switch to production, make a copy of the file - credentials.php - add the customers account number and license key information where commented, and replace the old file with the updated file on the server.

Select Store -> Configuration -> AvaTax Calc - Sales Tax settings

a) Replace the Development Company Code provided with the development account, with the company code created for the production company

b) Select 'Production' in the field: Select Development or Production

Save the form.


NB: Do check with the sales tax administrator that any custom tax codes, and Nexus configurations, created in the development account have been configured for the company created in the production account.

It is suggested that customers set up two companies in their production AvaTax account - a test company for test transactions - and a production company for normal operations.

ADDRESS VALIDATION
==================
We have removed the administrative option to block an order if the address entered by a user does not provide a valid sales tax calculation.

It is the responsibility of the site developer to handle the AvaTax error generated if the address fields are populated with invalid data by the user. 

The following address will generate an error:

Street: Nowhere Street
City: Nowhere City
Zip: 99999

AvaTax error: JurisdictionNotFoundError: Unable to determine the taxing jurisdictions.

We will investigate integrating this capability of the AvaTax service into new customer account creation - where it might make betters sense, including correcting/extending the address where applicable.

UPGRADE FROM EARLIER VERSION
============================
Please make sure that any customization of the module, and the account number and license key in the credentials.php file are saved prior to doing an upgrade.

The AvaTax module stores data in the table called "variable"

Earlier versions of the module were supplied without the function - hook_uninstall - used to remove the table values when the module is uninstalled.

We suggest you uninstall your current version of the module in one of two ways:

Option 1 - User latest version of the file - commerce_avatax.install
--------
1a) Save the production account number and license key - found in credentials.php - in a safe place - not required for development.
 
1b) Replace the ORIGINAL file - commerce_avatax.install - installed with the earlier module - with the latest version.

// $Id: commerce_avatax.install,v 3.1 2012/03/09 18:00:00 adTumbler Exp $

1c) Disable the module using the regular Drupal Admin

1d) Uninstall the module using the regular Drupal Admin - the new .install file will delete the AvaTax values in table 'variable' when the module is uninstalled.

1e) Delete the module folder "commerce_avatax" - NB: all files and sub-folders (everything) from your server.

1f) Install the latest version of the module

1g) Update the new version of the file - credentials.php - with the production account number and license key noted in step 1a)

1h) Configure module 

Option 2 - Manual database clean up
-------- 
2a) Save the production account number and license key - found in credentials.php - in a safe place - not required for development.

2b) Disable the module using the regular Drupal Module Administration

2c) Uninstall the module using the regular Drupal Admin 

2d) Delete the module folder "commerce_avatax" - NB: all files and sub-folders (everything) from your server.

2e) Delete all entries containing "commerce_avatax" from the table "variable"

2f) Install the latest version of the module

2g) Update the new version of the file - credentials.php - with the production account number and license key noted in step 2a) 

2h) Configure module

Open Issues
===========
1) If you are testing the module, - admin -> order -> edit - and you have a partially completed order in your cart (a use case that will not happen in practise as administrators will not also be shopping on the site) you will get unpredicatable results. The module will see both the partially completed order, and the order you are updating as valid sources for the hook to be applied to.

2) The module does not support creating a new order using the order admin form

3) The module does not support adding a new line to an existing order using the order admin form

4) The module partially supports a price update using the order admin form. You will need to manually adjust the selling price of the affected line item in AvaTax, which will correct the sales tax amount for that line item in AvaTax.



