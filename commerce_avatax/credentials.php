<?php

// ATConfig object is how credentials are set
// Tax or Address Service Objects take an argument
// which is the name of the ATConfig object ('Development' or 'Production' below)


/* This is a configuration called 'Development'.
 * It includes the account number and license key for the TRIAL use of the AvaTax service:
 *
 * $service = new AddressServiceSoap('Development');
 * $service = new TaxServiceSoap('Development');
 */
new ATConfig('Development', array(
    'url'       => 'https://development.avalara.net',
    'account'   => '1100090472', //replace with Avalara development account number
    'license'   => '8E21F38DCF89E5BB', //replace with Avalara development license key
    'client'    => 'DrupalCommerceAvaTax-adTumblerLLC,1.2',
    'name'    => 'DrupalCommercePHPConnector',
    'trace'     => true) // true for development
);

/* This is a configuration called 'Production'
 * Example:
 *
 * $service = new AddressServiceSoap('Production');
 * $service = new TaxServiceSoap('Production');
 */
new ATConfig('Production', array(
    'url'       => 'https://avatax.avalara.net',
    'account'   => '1100097242',  // Insert Avalara production account number
    'license'   => 'F7B00357FEA62812',  // Insert Avalara production license key
    'client'    => 'DrupalCommerceAvaTax-adTumblerLLC,1.2',
    'name'    => 'DrupalCommercePHPConnector',
    'trace'     => true) // false for production
);